Source: libfile-wildcard-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Florian Schlichting <fsfs@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libmodule-optional-perl,
                     perl
Standards-Version: 4.1.5
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libfile-wildcard-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libfile-wildcard-perl.git
Homepage: https://metacpan.org/release/File-Wildcard

Package: libfile-wildcard-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libmodule-optional-perl
Description: Enhanced glob processing
 When looking at how various operating systems do filename wildcard expansion
 (globbing), VMS has a nice syntax which allows expansion and searching of
 whole directory trees. It would be nice if other operating systems had
 something like this built in. The best Unix can manage is through the utility
 program find.
 .
 File::Wildcard provides this facility to Perl. Whereas native VMS syntax uses
 the ellipsis "...", this will not fit in with POSIX filenames, as ... is a
 valid (though somewhat strange) filename. Instead, the construct "///" is
 used as this cannot syntactically be part of a filename, as you do not get
 three concurrent filename separators with nothing between (three slashes are
 used to avoid confusion with //node/path/name syntax).
 .
 You don't have to use this syntax, as you can do the splitting yourself and
 pass in an arrayref as your path.
